#!/usr/bin/env zsh

# Ask for the administrator password upfront
sudo -v

# Keep-alive: update existing `sudo` time stamp until `.macports` has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# basic necessities
sudo port -N install zsh \
    +mp_completion \
    curl \
    curl-ca-bundle \
    coreutils \
    cmake \
    git \
    git-lfs \
    gnupg2 \
    gnuplot \
    gnutls \
    most \
    git \
    curl \
    wget \
    bash-completion \
    zsh-completions

# for code compilation
sudo port -N install gcc9 \
    cctools

# some programming languages
sudo port -N install go
sudo port -N install perl5
sudo port -N install qt5

# additional stuff
sudo port -N install vtk
sudo port -N install tcl
sudo port -N install tk

# python
sudo port -N install python27 \
    py27-pip \
    python2_select
sudo port -N install python37 \
    py37-pip \
    python3_select
sudo port -N install python38 \
    py38-pip \
    python3_select
sudo port -N install python39 \
    py39-pip \
    python3_select
sudo port -N install pip_select

# setting defaults
sudo port select --set python python38
sudo port select --set python2 python27
sudo port select --set python3 python38
sudo port select --set pip pip38
sudo port select --set pip2 pip27
sudo port select --set pip3 pip38
